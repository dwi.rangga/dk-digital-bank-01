import hapi = require('@hapi/hapi');

import userService from '../user.service';
import userController from '../user.controller';
import { IUser } from '../user.interface';
jest.mock('../user.service', () => ({
  getUsers: jest.fn(),
  createUser: jest.fn()
}));
let server: hapi.Server;

describe('user.controller', () => {
  beforeAll(async () => {
    server = new hapi.Server();
    server.route(userController);
  });

  describe('GET /users', () => {
    it('should return userService getUsers with code 200', async () => {
      const testUsers = [
        {
          _id: '5d29b6716394dea3588023d4',
          name: 'mahesh',
          age: 20,
          __v: 0,
          id: '5d29b6716394dea3588023d4'
        }
      ];
      userService.getUsers.mockResolvedValueOnce(testUsers);
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'GET',
        url: `/users`
      });
      expect(result.statusCode).toBe(200);
      expect(result.result).toEqual(testUsers);
      expect(userService.getUsers).toHaveBeenCalledTimes(1);
    });
  });

  describe('POST /users', () => {
    it('should return error on empty name', async () => {
      const testUser: IUser = {
        name: '',
        age: 20
      };
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/users`,
        payload: testUser
      });

      expect(result.statusCode).toBe(400);
      expect(result.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return error on not enough age', async () => {
      const testUser: IUser = {
        name: 'test',
        age: 10
      };
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/users`,
        payload: testUser
      });

      expect(response.statusCode).toBe(400);
      expect(response.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return 201 on valid payload and success service call', async () => {
      const testUser: IUser = {
        name: 'test',
        age: 18
      };

      userService.createUser.mockResolvedValueOnce({
        ...testUser,
        id: 'new id'
      });

      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/users`,
        payload: testUser
      });

      expect(response.statusCode).toBe(201);
      expect(response.result).toEqual({
        ...testUser,
        id: 'new id'
      });
    });
  });
});
