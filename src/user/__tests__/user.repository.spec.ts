import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

import { UserModel } from '../user.model';
import userRepository from '../user.repository';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('user.repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterAll(async () => {
    mongoose.disconnect();
    mongod.stop();
  });

  describe('get', () => {
    it('should get all users', async () => {
      await UserModel.create(
        {
          name: 'u1'
        },
        {
          name: 'u2'
        }
      );

      const users = await userRepository.get();
      expect(users).toHaveLength(2);
    });
  });

  describe('create', () => {
    it('should create new user', async () => {
      const newUser = await userRepository.create({
        name: 'new user',
        age: 10
      });
      expect(newUser.id).toBeDefined();
    });
  });

  describe('getFirstByName', () => {
    it('should return first user if name exists in db', async () => {
      await UserModel.create({
        name: 'exist name'
      });
      const user = await userRepository.getFirstByName('exist name');
      expect(user.name).toEqual('exist name');
    });

    it('should return null if name not exists in db', async () => {
      const user = await userRepository.getFirstByName('not exist name');
      expect(user).toBeNull();
    });
  });
});
