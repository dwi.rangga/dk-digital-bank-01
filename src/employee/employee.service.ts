import { ERROR_CODE } from '../common/errors';

import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';

const getEmployees = () => {
  return employeeRepository.get();
};

const createEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.EMPLOYEE_NAME_EXISTED);
  }
  return employeeRepository.create(employee);
};

const employeeService = {
  getEmployees,
  createEmployee
};
export default employeeService;
