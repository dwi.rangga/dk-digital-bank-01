import * as Joi from '@hapi/joi';

import employeeConstant from './employee.constant';

import { MongooseBase } from '../common/validators';

const EmployeeValidator = {
  name: Joi.string()
    .trim()
    .required(),
  age: Joi.number()
    .min(employeeConstant.MIN_EMPLOYEE_AGE)
    .required()
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeValidator
})
  .required()
  .label('Response - Employee');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response - Employee');

const createEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - new employee');

export {
  EmployeeResponseValidator,
  createEmployeeRequestValidator,
  EmployeeListResponseValidator,
  EmployeeValidator
};
