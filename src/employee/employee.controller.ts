import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  createEmployeeRequestValidator,
  EmployeeResponseValidator
} from './employee.validator';
import { IEmployeeRequest } from './employee.interface';

const getEmployee: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employees',
    notes: 'Get the list of employees',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};
const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new employee',
    notes: 'All information must valid',
    validate: {
      payload: createEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [getEmployee, createEmployee];
export default employeeController;
