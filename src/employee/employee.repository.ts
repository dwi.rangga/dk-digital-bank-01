import { EmployeeModel, EmployeeDocument } from './employee.model';
import { IEmployee, IEmployeeResponse } from './employee.interface';

const employeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const employeeDocumentsToObjects = (documents: EmployeeDocument[]) =>
  documents.map(employeeDocumentToObject);

const get = async () => {
  const documents = await EmployeeModel.find().exec();
  return employeeDocumentsToObjects(documents);
};

const create = async (employee: IEmployee) => {
  const newEmployee = new EmployeeModel(employee);

  await newEmployee.save();

  return employeeDocumentToObject(newEmployee);
};

const getFirstByName = async (name: string) => {
  const employee = await EmployeeModel.findOne({ name }).exec();
  return employee && employeeDocumentToObject(employee);
};

const employeeRepository = {
  get,
  create,
  getFirstByName
};
export default employeeRepository;
