import employeeService from '../employee.service';
import employeeRepository from '../employee.repository';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

jest.mock('../employee.repository');

describe('employeeService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getEmployees', () => {
    it('should return repository get result', async () => {
      const employeesTest = [
        {
          name: 'test'
        }
      ];
      employeeRepository.get.mockResolvedValueOnce(employeesTest);
      const employees = await employeeService.getEmployees();
      expect(employees).toEqual(employeesTest);
    });
  });

  describe('createEmployee', () => {
    it('should throw error if repository find existing employee with same name', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce({
        name: 'test'
      });
      try {
        await employeeService.createEmployee(employeeInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.EMPLOYEE_NAME_EXISTED);
      } finally {
        expect(employeeRepository.create).not.toBeCalled();
      }
    });

    it('should call repository create employee', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce(null);
      await employeeService.createEmployee(employeeInfo);
      expect(employeeRepository.create).toBeCalledWith(employeeInfo);
    });
  });
});
