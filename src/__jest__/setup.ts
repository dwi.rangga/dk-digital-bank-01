import { createNamespace, destroyNamespace } from 'cls-hooked';
import { Tracing } from '../common/constant';
afterEach(() => {
  expect.hasAssertions();
});
const session = Tracing.TRACER_SESSION;
beforeAll(() => {
  createNamespace(session);
});
afterAll(() => {
  try {
    destroyNamespace(session);
  } catch {}
});
