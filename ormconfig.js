/**
 * this is only used by TypeORM cli to run migration scripts
 */
const { config } = require('./src/config');
const { host, port, username, type, database } = config.get('db');
module.exports = {
  host,
  port,
  username,
  password: process.env.SQL_PASSWORD,
  type,
  database,
  synchronize: false,
  migrations: ['migrations/**/*.ts', 'migrations/**/*.js'],
  cli: {
    migrationsDir: 'migrations'
  }
};
